const Sequelize = require('sequelize');
var sequelize = new Sequelize
                (
                    'dacolrjf3emjan', 
                    'wcwzonobkltvvb', 
                    '21bfa601c83d5824ace92e6782986b45324b7e4861890c3ff9fb176204910d87', 
                    {
                        host: 'ec2-107-21-94-185.compute-1.amazonaws.com',
                        dialect: 'postgres',
                        port: 5432,
                        dialectOptions: {
                            ssl: true
                        }
                    }
                );


var Employee = sequelize.define('Employee', {
    employeeNum: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    firstName: Sequelize.STRING,
    lastName: Sequelize.STRING,
    email: Sequelize.STRING,
    SSN: Sequelize.STRING,
    addressStreet: Sequelize.STRING,
    addressCity: Sequelize.STRING,
    addressState: Sequelize.STRING,
    addressPostal: Sequelize.STRING,
    maritalStatus: Sequelize.STRING,
    isManager: Sequelize.BOOLEAN,
    employeeManagerNum: Sequelize.INTEGER,
    status: Sequelize.STRING,
    hireDate: Sequelize.STRING
});

var Department = sequelize.define('Department', {
    departmentId: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    departmentName: Sequelize.STRING
});

Department.hasMany(Employee, { foreignKey: 'department' });

module.exports.initialize = function () {
    return new Promise(function (resolve, reject) {
        sequelize
            .authenticate()
            .then(function () {
                sequelize
                    .sync()
                    .then(function () {
                        resolve();
                        return;
                    })
                    .catch(function (err) {
                        reject("unable to sync the database");
                        return;
                    });
            })
            .catch(function (err) {
                console.log('Unable to connect to the database:', err);
            });
    });
}

module.exports.getAllEmployees = function () {
    return new Promise(function (resolve, reject) {

        Employee.findAll().then(employees=>{
            resolve(employees);
            return;
        }).catch(error => {
            reject("no results returned");
            return;
        })
    });
}

module.exports.getEmployeesByDepartment = function (departmentId) {
    return new Promise(function (resolve, reject) {
        Employee.findAll({
            where:{
                department:departmentId
            }
        })
        .then(employees => {
            resolve(employees);
            return;
        })
        .catch(error => {
            reject("no results returned");
            return;
        })
    });
}

module.exports.getEmployeeByNum = function (num) {
    return new Promise(function (resolve, reject) {
        Employee.findAll({
            where:{
                employeeNum: num
            }
        })
        .then(employee => {
            resolve(employee[0]);
            return;
        })
        .catch(error => {
            reject("no results returned");
            return;
        })
    });
}

module.exports.getDepartments = function () {
    return new Promise(function (resolve, reject) {
        Department.findAll()
        .then(departments => {
            resolve(departments);
            return;
        })
        .catch(error => {
            reject("no results returned");
            return;
        })
    });
}

module.exports.addEmployee = function (employeeData) {

    return new Promise(function (resolve, reject) {

        for(const prop in employeeData) {
            if(employeeData[prop] == "")
                employeeData[prop] = null;
        }
        //console.log(employeeData);

        Employee.create({
            firstName: employeeData.firstName,
            lastName: employeeData.lastName,
            email: employeeData.email,
            SSN: employeeData.SSN,
            addressStreet: employeeData.addressStreet,
            addressCity: employeeData.addressCity,
            addressState: employeeData.addressState,
            addressPostal: employeeData.addressPostal,
            maritalStatus: employeeData.maritalStatus,
            isManager: employeeData.isManager,
            employeeManagerNum: employeeData.employeeManagerNum,
            status: employeeData.status,
            hireDate: employeeData.hireDate,
            department: employeeData.department

        }).then(function(emp){
            resolve("success");
            return;
        }).catch(function(error){
            reject("unable to create employee");
            return;
        })
    });
}

module.exports.updateEmployee = function (employeeData) {
    return new Promise(function (resolve, reject) {

        for(const prop in employeeData) {
            if(employeeData[prop] == "")
                employeeData[prop] = null;
        }

        //console.log(employeeData);
        
        Employee.update({
            firstName: employeeData.firstName,
            lastName: employeeData.lastName,
            email: employeeData.email,
            SSN: employeeData.SSN,
            addressStreet: employeeData.addressStreet,
            addressCity: employeeData.addressCity,
            addressState: employeeData.addressState,
            addressPostal: employeeData.addressPostal,
            maritalStatus: employeeData.maritalStatus,
            isManager: employeeData.isManager,
            employeeManagerNum: employeeData.employeeManagerNum,
            status: employeeData.status,
            hireDate: employeeData.hireDate,
            department: employeeData.department 
        }, {
            where: {
                employeeNum: employeeData.employeeNum
            }
        }).then(()=>{
            resolve();
            return;
        }).catch(()=>{
            reject("unable to update employee");
            return;
        })

    });
}

module.exports.getManagers = function () {
    return new Promise(function (resolve, reject) {
        reject();
    });
}

module.exports.addDepartment = function(departmentData) {
    return new Promise(function (resolve, reject) {

        for(const prop in departmentData) {
            if(departmentData[prop] == "")
                departmentData[prop] = null;
        }
        //console.log(departmentData);

        Department.create({
            departmentName: departmentData.departmentName
        }).then(function(emp){
            resolve("success");
            return;
        }).catch(function(error){
            reject("unable to create department");
            return;
        })
    });
}

module.exports.updateDepartment = function(departmentData) {
    return new Promise(function (resolve, reject) {

        for(const prop in departmentData) {
            if(departmentData[prop] == "")
                departmentData[prop] = null;
        }

        //console.log(departmentData);    
        Department.update({
            departmentName: departmentData.departmentName,
        }, {
            where: {
                departmentId: departmentData.departmentId
            }
        }).then(()=>{
            resolve();
            return;
        }).catch(()=>{
            reject("unable to update department");
            return;
        })

    });
}

module.exports.getDepartmentById = function (id) {
    return new Promise(function (resolve, reject) {
        Department.findAll({
            where:{
                departmentId: id
            }
        })
        .then(department => {
            resolve(department[0]);
            return;
        })
        .catch(error => {
            reject("no results returned");
            return;
        })
    });
}

module.exports.deleteDepartmentById = function(id) {
    return new Promise(function (resolve, reject) {
        Department.destroy({
                where: { 
                        departmentId: id 
                    }    
        }).then(success => { 
            resolve("successfully removed");
            return;
        }).catch(error => {
            reject("error encountered");
            return;
        })
    })
}

module.exports.deleteEmployeeByNum = function(empNum) {
    return new Promise(function (resolve, reject) {
        Employee.destroy({
                where: { 
                        employeeNum: empNum 
                    }    
        }).then(success => { 
            resolve("successfully removed");
            return;
        }).catch(error => {
            reject("error encountered");
            return;
        })
    })
}
