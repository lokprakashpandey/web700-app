/********************************************************************************* 
 * WEB700 – Assignment 06 
 * I declare that this assignment is my own work in accordance with Seneca Academic Policy. No part 
 * of this assignment has been copied manually or electronically from any other source 
 * (including 3rd party web sites) or distributed to other students. 
 * 
 * Name: Lok Prakash Pandey Student ID: 156066177 Date: 11/27/2019 
 * 
 * Online (Heroku) Link: https://powerful-tundra-05502.herokuapp.com/ 
 * 
 ********************************************************************************/
var HTTP_PORT = process.env.PORT || 8080;
var express = require("express");
var app = express();
var serverData = require("./modules/serverDataModule.js");
var path = require('path');
var exphbs = require("express-handlebars");
var bodyParser = require('body-parser');

app.engine('.hbs', exphbs({
    extname: '.hbs',
    defaultLayout: 'main',
    helpers: {

        navLink: function (url, options) {
            return '<li' +
                ((url == app.locals.activeRoute) ? ' class="nav-item active" ' : ' class="nav-item" ') +
                '><a class="nav-link" href="' + url + '">' + options.fn(this) + '</a></li>';
        },
        equal: function (lvalue, rvalue, options) {
            if (arguments.length < 3)
                throw new Error("Handlebars Helper equal needs 2 parameters");

            if (lvalue != rvalue) {
                return options.inverse(this);
            } else {
                return options.fn(this);
            }
        }
    }
}));

app.set('view engine', '.hbs');

app.use(express.static("public"));

app.use(bodyParser.urlencoded({ extended: true }));

app.use(function (req, res, next) {
    let route = req.baseUrl + req.path;
    app.locals.activeRoute = (route == "/") ? "/" : route.replace(/\/$/, "");
    next();
});

app.get("/employees/add", (req, res) => {
    serverData.getDepartments().then(data => {
        res.render("addEmployee", { departments: data });
    }).catch(msg => {
        res.render("addEmployee", { departments: [] });
    });
});

app.get("/departments/add", (req, res) => {
    res.render("addDepartment.hbs");
});

app.post("/employees/add", (req, res) => {
    req.body.isManager = req.body.isManager ? true : false;
    serverData.addEmployee(req.body).then(success => {
        res.redirect("/employees");
    }).catch(msg => console.log(msg));
});

app.post("/departments/add", (req, res) => {
    serverData.addDepartment(req.body).then(success => {
        res.redirect("/departments");
    }).catch(msg => console.log(msg));
});

app.post("/employee/update", (req, res) => {
    req.body.isManager = req.body.isManager ? true : false;
    serverData.updateEmployee(req.body).then(() => {
        res.redirect("/employees");
    }).catch((err)=>{
        res.status(500).send("Unable to Update Employee");
    });
});

app.post("/department/update", (req, res) => {
    serverData.updateDepartment(req.body).then(() => {
        res.redirect("/departments");
    }).catch((err)=>{
        res.status(500).send("Unable to Update Department");
    });
});

app.get("/employees", (req, res) => {
    let d = req.query.department;
    if (d) {
            serverData.getEmployeesByDepartment(d).then(empWithDepartmentArray => {
                if(empWithDepartmentArray.length == 0)
                    res.json({ message: "no employees" });   
                else 
                    res.send(empWithDepartmentArray);
            }).catch(empWithDepartmentMessage => {
                res.json({ message: empWithDepartmentMessage });
            });
    }
    else {
        serverData.getAllEmployees().then(data => {
            if (data.length > 0)
                res.render("employees", { employees: data });
            else
                res.render("employees", { message: "no results" });
        }).catch(empMessage => {
            res.render("employees", { message: empMessage });
        });
    }
});

app.get("/employee/:empNum", (req, res) => {
    // initialize an empty object to store the values
    let viewData = {};
    serverData.getEmployeeByNum(req.params.empNum).then((data) => {
        if (data) {
            viewData.employee = data; //store employee data in the "viewData" object as "employee"
        } else {
            viewData.employee = null; // set employee to null if none were returned
        }
    }).catch(() => {
        viewData.employee = null; // set employee to null if there was an error
    }).then(serverData.getDepartments()
        .then((data) => {
            viewData.departments = data; 
            // store department data in the "viewData" object as "departments"
            // loop through viewData.departments and once we have found the departmentId that matches
            // the employee's "department" value, add a "selected" property to the matching
            // viewData.departments object
            for (let i = 0; i < viewData.departments.length; i++) {
                if (viewData.departments[i].departmentId == viewData.employee.department) {
                    viewData.departments[i].selected = true;
                }
            }

        }).catch(() => {
            viewData.departments = []; // set departments to empty if there was an error
        }).then(() => {
            if (viewData.employee == null) { // if no employee - return an error
                res.status(404).send("Employee Not Found");
            } else {
                res.render("employee", { viewData: viewData }); // render the "employee" view
                //console.log(viewData.departments);
            }
        }));
});

app.get("/departments", (req, res) => {
    serverData.getDepartments().then(data => {
        if (data.length > 0)
            res.render("departments", { departments: data });
        else
            res.render("departments", { message: "no results" });
    }).catch(deptMessage => {
        res.render("departments", { message: deptMessage });
    });
});

app.get("/department/:id", (req, res) => {
    serverData.getDepartmentById(req.params.id).then(data => {

        if (data == undefined)
            res.status(404).send("Department Not Found");
        else
            res.render("department", { department: data });
    }).catch(deptByIdMessage => {
        res.json({ message: deptByIdMessage });
    });
});

app.get("/departments/delete/:departmentId", (req, res) => {
    serverData.deleteDepartmentById(req.params.departmentId).then(success => {
        res.redirect("/departments");
    }).catch(msg => {
        res.status(500).send("Unable to Remove Department / Department not found");
    });
})

app.get("/employees/delete/:empNum", (req, res) => {
    serverData.deleteEmployeeByNum(req.params.empNum).then(success => {
        res.redirect("/employees");
    }).catch(msg => {
        res.status(500).send("Unable to Remove Employee / Employee not found");
    });
})

app.get("/", (req, res) => {
    res.render("home.hbs");
});

app.get("/about", (req, res) => {
    res.render("about.hbs");
});

app.get("/htmlDemo", (req, res) => {
    res.render("htmlDemo.hbs");
});

app.get('*', function (req, res) {
    res.status(404).sendFile(path.join(__dirname, "/public/404.html"));
});

serverData.initialize().then(() => {
    app.listen(HTTP_PORT, () => {
        console.log("server listening on port: " + HTTP_PORT)
    });
}).catch((errMessage) => {
    console.log({ message: errMessage });
});
